const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update/:id', (req, res) => {
    let found = false;
    
    products.forEach((product, index) => {
        if (!found)
        {
            if (product.id == req.params.id)
            {
                found = true;
                products[index].productName = req.body.productName;
                products[index].price = req.body.price;
                
                res.status(200).send('The product has been updated.');
            }
            else
                res.status(500).send("This product doesn't exist.");
        }
    });
});

app.delete('/delete', (req, res) => {
    let found = false;
    
    products.forEach((product, index) => {
        if (!found)
        {
            if (product.productName == req.body.productName)
            {
                found = true;
                products.splice(index, 1);
                
                res.status(200).send('The product has been deleted.');
            }
            else
                res.status(500).send("This product doesn't exist.");
        }
    });
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});