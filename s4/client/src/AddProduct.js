import React, { Component } from 'react';
import axios from 'axios';

class AddProduct extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            newProductBody : {
                productName: 'New product name',
                price: 4999
            }
        };
    }
    
    addProduct()
    {
        axios.post('https://myworkspace-andreea18.c9users.io/add', this.state.newProductBody)
             .then(response => console.log(response))
             .catch(error => console.log(error))
    }
    
    render() {
        return (
            <div>
               <button onClick={ this.addProduct() }>Click to add new product</button>
            </div>
        );
    }
}

export default AddProduct;
