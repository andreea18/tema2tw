import React, { Component } from 'react';

class ProductList extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            products : []
        };
    }
    
    componentWillMount()
    {
        fetch('https://myworkspace-andreea18.c9users.io/get-all')
          .then(
            function(response) {
              if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' + response.status);
                return;
              }
        
              response.json().then(function(data) {
                    this.setState({ products: data })
              });
            }
          )
          .catch(function(err) {
            console.log('Fetch Error :-S', err);
          });
    }
    
    renderProducts(list)
    {
        const products = list.map((product, index) => {
            return (
                <div key={index}>
                    <p>Name: { product.productName}</p>
                    <p>Price: { product.price}</p>
                </div>
            );
        }); 
        
        return products;
    }
    
    
    render() {
        return (
            <div class="products">
                { this.renderProducts() }
            </div>
        );
    }
}

export default ProductList;
